<?php
//if uninstall not called from WordPress exit
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    die();
}

global $wpdb;
/* @var wpdb $wpdb */

if ( ! class_exists( 'WP_Code_Challenge' ) ) {

    //delete plugin options
    if ( is_multisite() )
        $wpdb->query( "DELETE FROM " . $wpdb->sitemeta . " WHERE meta_key LIKE '%wpcc_%' " );
    else
        $wpdb->query( "DELETE FROM " . $wpdb->options . " WHERE option_name LIKE '%wpcc_%' " );
}
