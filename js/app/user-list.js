const template =
    '<table class="table table-striped">' +
        '<thead>' +
            '<tr>' +
                '<th>ID</th>' +
                '<th>Username</th>' +
                '<th>Name</th>' +
                '<th></th>' +
            '</tr>' +
        '</thead>' +
        '<tbody>' +
            '<tr v-for="item in items">' +
                '<td>{{ item.id }}</td>' +
                '<td>{{ item.username }}</td>' +
                '<td>{{ item.name }}</td>' +
                '<td><button class="btn btn-primary" v-on:click="showDetail(item.id)">View</button></td>' +
            '</tr>' +
        '</tbody>' +
    '</table>';

  Vue.component('user-list', {
        props: ['items'],
        template: template,
        methods: {
            showDetail: function (id) {
                 this.$root.fetchDetail(id);
            }
        }
  });
