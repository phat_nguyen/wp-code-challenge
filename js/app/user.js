var user = Vue.component('user', {
    props: ['user'],
    template:
    '<div>' +
        '<p><b>Username:</b> <span v-if="user">{{ user.username }}</span></p>' +
        '<p><b>Name:</b> <span v-if="user">{{ user.name }}</span></p>' +
        '<p><b>Email:</b> <span v-if="user">{{ user.email }}</span></p>' +
        '<p><b>Phone:</b> <span v-if="user">{{ user.phone }}</span></p>' +
        '<p><b>Website:</b> <span v-if="user">{{ user.website }}</span></p>' +
        '<p><b>Address:</b> <span v-if="user">{{ user.address.street }}, {{ user.address.suite }}, {{ user.address.city }}, {{ user.address.zipcode }}</span></p>' +
        '<p><b>Company:</b> <span v-if="user">{{ user.company.name }}</span></p>' +
    '</div>',
});
