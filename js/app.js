import './app/user-list.js';
import './app/user.js';
var app = new Vue({
    el: '#app',
    props: ['user', 'items'],
    methods: {
        fetchDetail: function (id) {
            var url = 'https://jsonplaceholder.typicode.com/users/' + id;
            var user = JSON.parse(localStorage.getItem('user_' + id));

            if (user !== null) {
                this.user = user;
                return;
            }

            var self = this;
            fetch (url)
                .then((resp) => resp.json())
                .then(function(data) {
                    self.user = data;
                    localStorage.setItem('user_' + id, JSON.stringify(data));
                })
                .catch(function(e) {
                    console.log(e);
                });
        }
    },
    created: function () {
        var url = 'https://jsonplaceholder.typicode.com/users';
        var self = this;
        localStorage.clear();

        fetch (url)
            .then((resp) => resp.json())
            .then(function(data) {
                self.items = data;
            })
            .catch(function(e) {
                console.log(e);
            });
    }
});

