## Installation
- Install dependencies by running `composer install` in plugin folder
- Go to plugins page of Wordpress and active the plugin
- Endpoint can be changed in setting page, the default endpoint is `default-endpoint`

## Implementation
- Classes are autoloaded by composer in PSR-4 mode 
- Usage of Vuejs: the library can be embed directly into Wordpress and doesn't need server side to compile code
- Bootstrap CSS framework: for popularity and easy usage
- Users data are cached in localStorage to reduce number of API requests 

## Running test
- After install composer packages, go to plugin root folder and run
```text
php vendor/phpunit/phpunit/phpunit
```
