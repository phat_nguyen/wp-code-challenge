<?php
/**
 * Plugin Name: WP Code Challenge
 * Plugin URI: https://freelancer.com/u/vphat28.html
 * Description: Test plugin for WP VIP developer position
 * Author: Xavi Nguyen
 * Author URI: https://github.com/vphat28
 * Version: 1.0.0
 * Text Domain: wp-code-challenge
 * Domain Path: /languages
 * Network: true
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! defined( 'WPCCP' ) ) {
    define('WPCCP', __DIR__);
}

use WP_Code_Challenge\Bootstrap;
use WP_Code_Challenge\Admin\Setting;

if ( ! class_exists( 'WP_Code_Challenge', false ) ) {
    final class WP_Code_Challenge {
        const TEXT_DOMAIN = 'wp-code-challenge';

        private static $instance = null;

        private $base_url;

        /**
         * Set needed filters and actions and load
         */
        private function __construct() {
            add_action( 'init', array( Bootstrap::class, 'get_instance' ) );
            $this->base_url = plugins_url( '', __FILE__ );
            add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'plugin_action_links' ) );

            if (file_exists( __DIR__ . '/vendor/autoload.php')) {
                require_once __DIR__ . '/vendor/autoload.php';
            }

            \WP_Code_Challenge\Template::get_instance( )->set_base_url( $this->base_url );
            \WP_Code_Challenge\Template::get_instance( )->set_base_path( __DIR__ );

            if ( is_admin() ) {
                Setting::get_instance( );
            }
        }

        /**
         * Adds plugin action links.
         */
        public function plugin_action_links( $links ) {
            $plugin_links = array(
                '<a href="options-general.php?page=wp-code-challenge-setting-admin">' . esc_html__( 'Settings', self::TEXT_DOMAIN ) . '</a>',
            );
            return array_merge( $plugin_links, $links );
        }

        /**
         * @return self
         */
        public static function get_instance() {
            if ( null === self::$instance ) {
                self::$instance = new self;
            }

            return self::$instance;
        }

        /**
         * Prevent Cloning
         */
        public function __clone() {
            wp_die( );
        }

        /**
         * Prevent deserialization
         */
        public function __wakeup() {
            wp_die( );
        }

        /**
         * Load Plugin Translation
         *
         * @return bool Text domain loaded
         */
        public static function load_text_domain() {
            if ( is_textdomain_loaded( self::TEXT_DOMAIN ) ) {
                return true;
            }

            return load_plugin_textdomain( self::TEXT_DOMAIN, false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
        }
    }

    //Start Plugin
    add_action( 'plugins_loaded', array( 'WP_Code_Challenge', 'get_instance' ), 8 );
}
