<?php

namespace WP_Code_Challenge\Admin;

use WP_Code_Challenge\AbstractClass;

class Setting extends AbstractClass
{
    const MENU_SLUG = 'wp-code-challenge-setting-admin';
    const MENU_TITLE = 'WP Code Challenge';
    const OPTION_GROUP = 'wpcc_option_group';
    const ENDPOINT_CONFIG = 'wpcc_endpoint';

    private $option;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin',
            __( self::MENU_TITLE ),
            'manage_options',
            self::MENU_SLUG,
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        $this->option = get_option( self::ENDPOINT_CONFIG );
        ?>
        <div class="wrap">
            <h1>My Settings</h1>
            <form method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields( self::OPTION_GROUP );
                do_settings_sections( self::MENU_SLUG );
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {
        register_setting(
            self::OPTION_GROUP,
            self::ENDPOINT_CONFIG,
            array( $this, 'sanitize' )
        );

        add_settings_section(
            'setting_section_id',
            __( self::MENU_TITLE ),
            array( $this, 'print_section_info' ),
            self::MENU_SLUG
        );

        add_settings_field(
            'title',
            __('Endpoint'),
            array( $this, 'endpoint_callback' ),
            self::MENU_SLUG,
            'setting_section_id'
        );
    }


    public function sanitize( $input )
    {
        return sanitize_text_field( $input );
    }

    /**
     * Print the Section text
     */
    public function print_section_info()
    {
        print __('Settings');
    }

    /**
     * Get the option and print one of its values
     */
    public function endpoint_callback()
    {
        printf(
            '<input type="text" id="title" name="' . self::ENDPOINT_CONFIG . '" value="%s" />',
            isset( $this->option ) ? esc_attr( $this->option ) : ''
        );
    }

    /**
     * @return mixed|string
     */
    public function get_end_point()
    {
        $config = get_option( Setting::ENDPOINT_CONFIG );

        if ( empty( $config ) ) {
            return 'default-endpoint';
        }

        return $config;
    }
}
