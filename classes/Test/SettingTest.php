<?php

namespace WP_Code_Challenge\Test;

use Brain\Monkey;
use WP_Code_Challenge\Admin\Setting;

class SettingTest extends \PHPUnit\Framework\TestCase
{

    protected function setUp()
    {
        parent::setUp();
        Monkey\setUp();
    }

    protected function tearDown()
    {
        Monkey\tearDown();
        parent::tearDown();
    }

    public function testAddHooksActuallyAddsHooks()
    {
        $obj = ( new Setting() );
        $this->assertTrue( has_action('admin_init', [$obj, 'page_init']) );
        $this->assertTrue( has_action('admin_menu', [$obj, 'add_plugin_page']) );
    }
}
