<?php

namespace WP_Code_Challenge\Test;

use Brain\Monkey;
use WP_Code_Challenge\Bootstrap;

class BootstrapTest extends \PHPUnit\Framework\TestCase
{
    protected function setUp()
    {
        parent::setUp();
        Monkey\setUp();
    }

    protected function tearDown()
    {
        Monkey\tearDown();
        parent::tearDown();
    }

    public function testAddFilters()
    {
        $obj = new Bootstrap();
        $this->assertTrue( has_filter( 'template_include', array( $obj, 'modify_template_path' ) ) );
    }
}
