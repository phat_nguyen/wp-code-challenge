<?php

namespace WP_Code_Challenge\Test;

use Brain\Monkey;
use Brain\Monkey\Filters;
use WP_Code_Challenge\Template;

class TemplateTest extends \PHPUnit\Framework\TestCase {

    protected function setUp()
    {
        parent::setUp();
        Monkey\setUp();
    }

    protected function tearDown()
    {
        Monkey\tearDown();
        parent::tearDown();
    }

    public function testAddFilters()
    {
        $obj = ( new Template() );
        $obj->get_file_url('somepath');
        $this->assertTrue( Filters\applied('wpccp_assets_path') > 0 );
    }
}
