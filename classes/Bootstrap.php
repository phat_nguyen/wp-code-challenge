<?php

namespace WP_Code_Challenge;

use WP_Code_Challenge\Admin\Setting;

class Bootstrap extends AbstractClass
{
    public function __construct()
    {
        add_filter( 'template_include' , array( $this, 'modify_template_path' ) );
    }

    /**
     * Check for target endpoint
     *
     * @return bool
     */
    public function check_end_point()
    {
        $endpoint = trim($_SERVER['REQUEST_URI'], '/');
        $config = Setting::get_instance()->get_end_point();

        if ( $endpoint === $config ) {
            return true;
        }

        return false;
    }

    /**
     * Load vuejs in footer
     */
    public function add_footer( )
    {
        $template = Template::get_instance();
        ?>
        <script src="https://unpkg.com/vue/dist/vue.js"></script>
        <script type="module" src="<?php echo $template->get_base_url() . '/js/app.js'; ?>"></script>
        <?php
    }

    /**
     * Check if endpoint is matched to alter the template path
     *
     * @param $path
     * @return string
     */
    public function modify_template_path( $path )
    {
        if ( !$this->check_end_point( ) ) {
            return $path;
        }

        $template = Template::get_instance();
        wp_enqueue_style( 'bootstrap', $template->get_base_url() . '/css/bootstrap.min.css' );
        add_action( 'wp_footer', array( $this, 'add_footer' ) );

        // check for theme's file
        if ( is_file( wp_get_theme( )->get_template_directory( ) . '/user-app.php' ) ) {
            return wp_get_theme( )->get_template_directory( ) . '/user-app.php';
        }

        return WPCCP . '/templates/' . 'app.phtml';
    }
}
