<?php

namespace WP_Code_Challenge;

class Template extends AbstractClass
{
    private $base_path;

    /** @var string */
    private $base_url;

    public function get_file_url( $path )
    {
        $path = $this->base_url . '/templates/' . $path;
        $path = apply_filters('wpccp_assets_path', $path);

        return $path;
    }

    public function get_base_url( )
    {
        return $this->base_url;
    }

    public function set_base_url( $url )
    {
        $this->base_url = $url;
    }

    public function set_base_path( $path )
    {
        $this->base_path = $path;
    }
}
