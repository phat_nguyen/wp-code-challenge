<?php

namespace WP_Code_Challenge;

abstract class AbstractClass
{
    protected static $instances = null;

    /**
     * @static
     * @return static
     */
    public static function get_instance() {
        if ( isset( self::$instances[static::class] ) ) {
            return self::$instances[static::class];
        }

        self::$instances[static::class] = new static();

        return self::$instances[static::class];
    }
}
